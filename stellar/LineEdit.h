#ifndef LINEEDIT_H
#define LINEEDIT_H

#include <QLineEdit>

using Validator = std::function<bool(const QString &)>;

class LineEdit: public QLineEdit {
    Q_OBJECT

public:
    using Super = QLineEdit;
    using This  = LineEdit;
    LineEdit(QWidget * = nullptr);
    void setValidator(const Validator &);
    void setValid(bool v) { valid = v; }
    bool isValid() const { return valid; }

private:
    bool valid;
    Validator validator;

private slots:
    void validate(const QString & text);

signals:
    void validationChanged(bool ok);
};

const Validator NonEmptyValidator = std::not_fn(&QString::isEmpty);

#endif // LINEEDIT_H
