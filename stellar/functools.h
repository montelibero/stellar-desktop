#ifndef FUNCTOOLS_H
#define FUNCTOOLS_H

#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>
#include <QList>

template <typename BinaryFunction>
struct Transformed2 {
    BinaryFunction const & f;
};

template <typename BinaryFunction>
Transformed2<BinaryFunction> transformed2(BinaryFunction const & f) {
    return {f};
}

template <typename Range, typename BinaryFunction>
auto operator|(Range const & range, Transformed2<BinaryFunction> const & t2) {
    return boost::adaptors::transform(
        range, [&t2](auto pair){return t2.f(pair.first, pair.second);}
    );
}

template <typename Collection, typename Range>
Collection collect(Range const & range) {
    Collection result;
    for (auto const & item : range)
        result.push_back(item);
    return result;
}

template <typename ResultCollection, typename A, typename Function>
ResultCollection map(Function const & f, QList<A> const & xs) {
    ResultCollection result;
    for (auto const & x : xs)
        result.push_back(f(x));
    return result;
}

template <typename ResultCollection, typename K, typename V, typename Function>
ResultCollection traverseWithKey(Function const & f, QMap<K, V> const & m) {
    ResultCollection result;
    for (auto i = m.cbegin(), end = m.cend(); i != end; ++i)
        result.push_back(f(i.key(), i.value()));
    return result;
}

template <typename K, typename V, typename Function>
void traverseWithKey_(Function const & f, QMap<K, V> const & m) {
    for (auto i = m.cbegin(), end = m.cend(); i != end; ++i)
        f(i.key(), i.value());
}

#endif // FUNCTOOLS_H
