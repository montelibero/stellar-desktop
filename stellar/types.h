#ifndef TYPES_H
#define TYPES_H

#include <QString>
#include <QList>

/// Number encoded in string /[0-9]+\.[0-9]{7}/
struct Currency {
    QString string;
};

struct Balance {
    Currency balance;
    QString asset_code;
    QString asset_issuer;

    QString shortAssetCode() const { return isNative() ? "XLM" : asset_code; }

    QString fullAssetCode() const {
        return isNative() ? "XLM" : asset_code + "-" + asset_issuer;
    }

    bool isNative() const { return asset_issuer.isEmpty(); }
};

/// Account DTO
struct Account {
    QList<Balance> balances;
    QString name;
    QString publicKey;
};

#endif // TYPES_H
