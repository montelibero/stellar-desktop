#ifndef DATABASE_H
#define DATABASE_H

// Qt
#include <QMap>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QString>

// StellarQtSDK
#include <responses/accountresponse.h>

// project
#include "types.h"

struct SqlQueryException {
    QString     executedQuery;
    QSqlError   lastError;
    QString     lastQuery;
};

class Database {
public:
    Database();
    ~Database();

    Account         addAccount(QString const & publicKey, QString const & name);
    void            deleteAccount(Account const &);
    QList<Account>  getAccounts();
    void            setAccountName(Account &, QString const & name);
    void            setBalances(Account &, QList<Balance> const &);

private:
//    QSqlDatabase cache;
    QSqlDatabase state;
};

#endif // DATABASE_H
