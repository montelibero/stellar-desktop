#include "RichButton.h"

struct RichButtonPimpl {
    QLabel * label;
};

RichButton::RichButton(QWidget * const parent)
    : Super{parent}, p{new RichButtonPimpl{new QLabel{this}}}
{
    setMinimumHeight(100);

    p->label->setAttribute(Qt::WA_TransparentForMouseEvents);
    p->label->setTextFormat(Qt::RichText);

    auto const box = new QVBoxLayout(this);
    box->setSizeConstraint(QLayout::SetMinimumSize);
    box->addWidget(p->label);
}

RichButton::~RichButton() {
}

void RichButton::setText(const QString & text) {
    p->label->setText(text);
}
