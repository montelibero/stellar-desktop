#include "LineEdit.h"

LineEdit::LineEdit(QWidget * const parent): Super(parent) {
    connect(this, &QLineEdit::textEdited, this, &This::validate);
}

void LineEdit::setValidator(const Validator & v) {
    validator = v;
    validate(text());
}

void LineEdit::validate(const QString & text) {
    bool wasValid = valid;
    valid = validator(text);
    if (wasValid != valid)
        emit validationChanged(valid);
}
