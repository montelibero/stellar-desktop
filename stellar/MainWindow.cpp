// Qt
#include <QLabel>
#include <QStyle>
#include <QToolBar>
#include <QVBoxLayout>

// project
#include "views/AccountListView.h"
#include "views/CreateAccountView.h"

// module
#include "MainWindow.h"

using namespace std;

const string_view StellarHorizonMainnetServerUrl{"https://horizon.stellar.org"};

const string_view StellarHorizonTestnetServerUrl{
    "https://horizon-testnet.stellar.org"
};

struct MainWindowPimpl {
    QVBoxLayout * walletTabLayout;
    MainWindowPimpl(): walletTabLayout{new QVBoxLayout} {}
};

MainWindow::MainWindow(bool const production):
    stellarHorizonServer{
        production
        ? StellarHorizonMainnetServerUrl.data()
        : StellarHorizonTestnetServerUrl.data()
    },
    p{new MainWindowPimpl}
{
    setupUi(production);
}

MainWindow::~MainWindow() {
}

void MainWindow::setupUi(bool const production) {
    resize(800, 600);
    setWindowTitle(
        tr("Stellar Desktop")
        + " - "
        + (production ? tr("Public network") : tr("Test network"))
    );
    setupToolBar();
    setCentralWidget(new AccountListView(this));
}

void MainWindow::setupToolBar() {
    auto const toolbar = addToolBar("");
    toolbar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    toolbar->setMovable(false);
    toolbar->addAction(
        style()->standardIcon(QStyle::SP_DesktopIcon),
        tr("Wallet"),
        this,
        [this]{ setCentralWidget(new AccountListView(this)); }
    );
    toolbar->addAction(
        style()->standardIcon(QStyle::SP_DriveCDIcon),
        tr("Address Generator"),
        this,
        [this]{ setCentralWidget(new CreateAccountView(this)); }
    );
}
