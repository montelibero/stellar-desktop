// std
#include <memory>
using std::shared_ptr;

// Qt
#include <QClipboard>
#include <QInputDialog>
#include <QMessageBox>
#include <QMimeData>
#include <QRegularExpressionValidator>

// StellarQtSQK
#include <keypair.h>

// project
#include "AccountView.h"

// module
#include "CreateAccountView.h"
#include "ui_CreateAccountView.h"

void copyPasswordToClipboard(QString const & text) {
    auto * mime = new QMimeData;
    mime->setText(text);
    mime->setData(
        #ifdef Q_OS_MACOS
            "application/x-nspasteboard-concealed-type", text.toUtf8()
        #elif defined(Q_OS_LINUX)
            "x-kde-passwordManagerHint", QByteArrayLiteral("secret")
        #elif defined(Q_OS_WIN)
            "ExcludeClipboardContentFromMonitorProcessing",
            QByteArrayLiteral("1")
        #endif
    );
    QApplication::clipboard()->setMimeData(mime);
}

class Base32Validator: public QValidator {
    virtual State validate(QString & input, int & pos) const override {
        QString result;
        for (const QChar c : input) {
            if ('2' <= c && c <= '7' || 'A' <= c && c <= 'Z')
                result += c;
            else if ('a' <= c && c <= 'z')
                result += c.toUpper();
            // others are dropped
        }
        input = result;
        return Acceptable;
    }
};

CreateAccountView::CreateAccountView(MainWindow * const window):
    QWidget{window},
    database{window->database},
    mode{Mode::GenerateOne},
    ui{new Ui::CreateAccountView},
    window{*window}
{
    ui->setupUi(this);
    ui->fixedPartType->addItem(tr("Ends with"), EndsWith);
    ui->fixedPartType->addItem(tr("Contains"), Contains);

    ui->fixedPart->setValidator(new Base32Validator);
    connect(
        ui->fixedPart,
        &QLineEdit::textChanged,
        this,
        [this](QString const & text){
            switchMode(
                text.isEmpty()
                ? Mode::GenerateOne
                : Mode::PrepareGenerateWithFixedPart
            );
        }
    );

    connect(
        ui->copyPublicKey,
        &QAbstractButton::clicked,
        this,
        [this]{ QApplication::clipboard()->setText(ui->publicKey->text()); }
    );

    connect(
        ui->copySecretKey, &QAbstractButton::clicked, this, &This::copySecretKey
    );

    ui->name->setValidator(NonEmptyValidator);
    connect(ui->name, &LineEdit::validationChanged, this, &This::updateUi);
    updateUi();

    connect(
        ui->generate, &QAbstractButton::clicked, this, &This::startStopGenerate
    );
    generateOne();

    connect(ui->accept, &QAbstractButton::clicked, this, &This::save);

    connect(
        &generator, &AddressGenerator::status, this, &This::showGeneratorStatus
    );
    connect(
        &generator, &AddressGenerator::done,
        this, [this](auto n, auto kp){
            showGeneratorStatus(n);
            switchMode(Mode::PrepareGenerateWithFixedPart);
            if (kp) {
                ui->publicKey->setText(kp->getAccountId());
                secretKey = kp->getSecretSeed();
            }
        }
    );
}

CreateAccountView::~CreateAccountView() {
    delete ui;
}

void CreateAccountView::copySecretKey() {
    copyPasswordToClipboard(secretKey);
}

void CreateAccountView::startStopGenerate() {
    switch (mode) {
        case Mode::GenerateOne:
            generateOne();
            break;
        case Mode::PrepareGenerateWithFixedPart:
            startGenerator(
                ui->fixedPartType->currentData().value<FixedPartType>(),
                ui->fixedPart->text()
            );
            break;
        case Mode::GeneratingWithFixedPart:
            generator.cancel();
            break;
        }
}

void CreateAccountView::generateOne() {
    shared_ptr<KeyPair> kp{KeyPair::random()};
    ui->publicKey->setText(kp->getAccountId());
    secretKey = kp->getSecretSeed();
}

void CreateAccountView::switchMode(Mode mode) {
    this->mode = mode;
    updateUi();
}

void
CreateAccountView::startGenerator(
    FixedPartType type, const QString & fixedPart
) {
    switchMode(Mode::GeneratingWithFixedPart);
    ui->publicKey->clear();
    generator.start(type, fixedPart);
}

void CreateAccountView::updateUi() {
    ui->generate->setText(
        mode == Mode::GenerateOne                  ? tr("Generate")         :
        mode == Mode::PrepareGenerateWithFixedPart ? tr("Start generating") :
        mode == Mode::GeneratingWithFixedPart      ? tr("Stop")             :
        throw
    );
    const QList<QWidget*> generatorWidgets{
        ui->timeLabel, ui->time, ui->addressesLabel, ui->addresses
    };
    for (const auto w : generatorWidgets)
        w->setVisible(mode != Mode::GenerateOne);

    // validation
    const bool nameIsValid = ui->name->isValid();
    ui->nameWarning->setVisible(!nameIsValid);
    ui->accept->setEnabled(
        mode != Mode::GeneratingWithFixedPart
        && nameIsValid
        && !ui->publicKey->text().isEmpty()
    );
}

void CreateAccountView::save() {
    QApplication::clipboard()->clear();

    bool ok;
    const auto answer =
        QInputDialog::getText(
            this, tr("Create account"),
            tr(
                "Have you saved the secret key in a safe place?"
                " Please enter it here to validate:"
            ),
            QLineEdit::Normal, "", &ok
        );
    if (!ok) { // dialog canceled
        return;
    }
    if (answer.trimmed() != secretKey) {
        QMessageBox::critical(
            this, tr("Create account"), tr("Secret key doesn't match")
        );
        return;
    }

    // ok
    auto account = database.addAccount(ui->publicKey->text(), ui->name->text());
    window.setCentralWidget(new AccountView(&window, account));
}

void CreateAccountView::showGeneratorStatus(unsigned int addressesChecked) {
    static QLocale locale;

    if (addressesChecked == 0)
        generatorTimer.start();

    const auto msecs = generatorTimer.elapsed();
    ui->time->setText(QTime::fromMSecsSinceStartOfDay(msecs).toString());
    const double speed = addressesChecked / (double(msecs) / 1000);
    ui->addresses->setText(
        locale.toString(addressesChecked)
        +   (   std::isfinite(speed)
                ? "; " + locale.toString(speed) + " addr/sec"
                : ""
            )
    );
}

void AddressGenerator::run() {
    const size_t KeyLength = 32;

    QByteArray seed{KeyLength, Qt::Uninitialized};
    QRandomGenerator randomDevice = QRandomGenerator::securelySeeded();
    shared_ptr<KeyPair> kp;
    unsigned count = 0;
    emit status(count);
    while (alive) {
        randomDevice
            .fillRange((quint32*)seed.data(), KeyLength / sizeof(quint32));
        kp.reset(KeyPair::fromSecretSeed(seed));
        ++count;
        const auto accountId = kp->getAccountId();
        if (
            type == EndsWith ? accountId.endsWith(fixedPart) :
            type == Contains ? accountId.contains(fixedPart) :
            throw
        ) {
            emit done(count, kp);
            return;
        }
        if (count % 1024 == 0)
            emit status(count);
    }
    if (!alive)
        emit done(count, nullptr);
}

void
AddressGenerator::start(FixedPartType const type, const QString & fixedPart) {
    this->type = type;
    this->fixedPart = fixedPart;
    alive = true;
    Super::start();
}

void AddressGenerator::cancel() {
    alive = false;
}
