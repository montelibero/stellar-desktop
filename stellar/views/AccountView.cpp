// Qt
#include <QInputDialog>
#include <QMessageBox>

// project
#include "AccountListView.h"
#include "functools.h"

// module
#include "AccountView.h"
#include "ui_AccountView.h"

using namespace std;

AccountView::AccountView(MainWindow * window, Account const & account):
    QWidget(window),
    account(account),
    ui(new Ui::AccountView),
    window(*window)
{
    ui->setupUi(this);
    ui->name->setText(account.name);
    ui->publicKey->setText(account.publicKey);
    connect(ui->back, &QAbstractButton::clicked, this, &This::goBack);
    connect(
        ui->deleteAccount, &QAbstractButton::clicked, this, &This::deleteAccount
    );
    connect(ui->refresh, &QAbstractButton::clicked, this, &This::refresh);
    connect(ui->rename, &QAbstractButton::clicked, this, &This::rename);

    setBalances(account.balances);
}

AccountView::~AccountView() {
    delete ui;
}

void AccountView::deleteAccount() {
    const auto answer =
        QMessageBox::question(
            this,
            tr("Delete account"),
            tr("Are you sure you want to delete this account?")
        );
    if (answer == QMessageBox::Yes) {
        window.database.deleteAccount(account);
        goBack();
    }
}

void AccountView::goBack() {
    window.setCentralWidget(new AccountListView(&window));
}

QList<Balance>
balancesFromResponse(QList<AccountResponseAttach::Balance> const & balances) {
    return ::map<QList<Balance>>(
        [](auto const & b) {
            return Balance{
                .balance        = Currency{b.getBalance()},
                .asset_code     = b.getAssetCode(),
                .asset_issuer   = b.assetIssuer(),
            };
        },
        balances
    );
}

template <typename Response>
void
onResponseReady(
    const Response * response, function<void(const Response *)> action
) {
    QObject::connect(
        response, &Response::ready, [=]{ action(response); delete response; }
    );
}

void AccountView::refresh() {
    const unique_ptr<const KeyPair> accountKeyPair {
        KeyPair::fromAccountId(account.publicKey)
    };
    ui->refresh->setEnabled(false);
    onResponseReady<AccountResponse>(
        window.stellarHorizonServer.accounts().account(accountKeyPair.get()),
        [this](auto response){
            ui->refresh->setEnabled(true);
            auto const balances{balancesFromResponse(response->getBalances())};
            window.database.setBalances(account, balances);
            setBalances(balances);
        }
    );
}

void AccountView::rename() {
    bool ok;
    const auto text =
        QInputDialog::getText(
            this, tr("Rename account"), tr("Account name:"), QLineEdit::Normal,
            ui->name->text(), &ok
        );
    if (ok && !text.isEmpty()) {
        window.database.setAccountName(account, text);
        ui->name->setText(text);
    }
}

void AccountView::setBalances(QList<Balance> const & balances) {
    delete ui->balances;
    ui->balances = new QFormLayout;
    static_cast<QBoxLayout*>(ui->balancesBox->layout())
        ->addLayout(ui->balances);
    for (auto const & balance : balances)
        ui->balances->addRow(
            balance.shortAssetCode(), new QLabel(balance.balance.string)
        );
}
