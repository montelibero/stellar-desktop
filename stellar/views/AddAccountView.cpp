// Qt
#include <QtWidgets>

// project
#include "CreateAccountView.h"
#include "ImportAccountView.h"

// module
#include "AddAccountView.h"
#include "ui_AddAccountView.h"

AddAccountView::AddAccountView(MainWindow * const window)
    : QWidget{window}
    , ui{new Ui::AddAccountView}
{
    ui->setupUi(this);
    connect(
        ui->createNewAccount,
        &QAbstractButton::clicked,
        window,
        [window]{ window->setCentralWidget(new CreateAccountView(window)); }
    );
    connect(
        ui->importAccount,
        &QAbstractButton::clicked,
        window,
        [window]{ window->setCentralWidget(new ImportAccountView(window)); }
    );
}

AddAccountView::~AddAccountView() {
    delete ui;
}
