// libs
#include <QtSql>

// project
#include "AccountView.h"
#include "AddAccountView.h"
#include "RichButton.h"
#include "functools.h"

// module
#include "AccountListView.h"
#include "ui_AccountListView.h"

QString assetTemplate(Balance const & b) {
    static const QString t = "%1 <b>%2</b>";
    return t.arg(b.balance.string, b.shortAssetCode());
};

QString accountButtonTemplate(Account const & account) {
    static const QString t = "<big><b>%1</b></big><br><br>%2";
    return t.arg(
        account.name,
        map<QStringList>(assetTemplate, account.balances).join("<br>")
    );
}

AccountListView::AccountListView(MainWindow * const window):
    QWidget(window),
    ui(new Ui::AccountListView)
{
    ui->setupUi(this);

    connect(
        ui->addAccountButton, &QAbstractButton::clicked,
        window, [window]{window->setCentralWidget(new AddAccountView(window));}
    );

    const auto accounts = window->database.getAccounts();
    for (const auto & account : accounts) {
        auto accountButton = new RichButton(this);
        accountButton->setText(accountButtonTemplate(account));
        ui->scrollAreaWidgetContents->layout()->addWidget(accountButton);
        connect(
            accountButton, &QAbstractButton::clicked,
            window,
            [window, account]{
                window->setCentralWidget(new AccountView(window, account));
            }
        );
    }
}

AccountListView::~AccountListView() {
    delete ui;
}
