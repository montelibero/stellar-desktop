#ifndef ACCOUNTLISTVIEW_H
#define ACCOUNTLISTVIEW_H

#include <QWidget>

#include "MainWindow.h"

namespace Ui {
    class AccountListView;
}

class AccountListView: public QWidget {
    Q_OBJECT

public:
    explicit AccountListView(MainWindow *);
    ~AccountListView();

private:
    Ui::AccountListView * ui;
};

#endif // ACCOUNTLISTVIEW_H
