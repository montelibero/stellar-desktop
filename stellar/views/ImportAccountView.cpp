// StellarQtSDK
#include <strkey.h>

// project
#include "AccountListView.h"
#include "LineEdit.h"

// module
#include "ImportAccountView.h"
#include "ui_ImportAccountView.h"

bool isValidPublicKeyBech32(const QString & s) {
    try {
        StrKey::decodeStellarAccountId(s);
        return true;
    } catch (const std::runtime_error &) {
        return false;
    }
}

ImportAccountView::ImportAccountView(MainWindow * const window):
    QWidget(window),
    database(window->database),
    ui(new Ui::ImportAccountView)
{
    ui->setupUi(this);

    ui->name->setValidator(NonEmptyValidator);
    connect(ui->name, &LineEdit::validationChanged, this, &This::revalidate);

    ui->publicKey->setValidator(isValidPublicKeyBech32);
    connect(
        ui->publicKey, &LineEdit::validationChanged, this, &This::revalidate
    );
    revalidate();

    connect(
        ui->save,
        &QAbstractButton::clicked,
        this,
        [this, window] {
            database.addAccount(ui->publicKey->text(), ui->name->text());
            window->setCentralWidget(new AccountListView(window));
        }
    );
}

ImportAccountView::~ImportAccountView() {
    delete ui;
}

void ImportAccountView::revalidate() {
    const bool isNameValid = ui->name->isValid();
    ui->nameWarning->setText(isNameValid ? "" : tr("Name must be set"));

    const bool isPublicKeyValid = ui->publicKey->isValid();
    ui  ->publicKeyWarning
        ->setText(isPublicKeyValid ? "" : tr("Public key must start with G"));

    ui->save->setEnabled(isNameValid && isPublicKeyValid);
}
