#ifndef CREATEACCOUNTVIEW_H
#define CREATEACCOUNTVIEW_H

#include <QString>
#include <QWidget>
#include <QtSql>

#include "MainWindow.h"

namespace Ui {
    class CreateAccountView;
}

enum FixedPartType {
    EndsWith,
    Contains,
};

class AddressGenerator: public QThread {
    Q_OBJECT
    using Super = QThread;

private:
    FixedPartType type;
    QString fixedPart;
    volatile bool alive;

    void run() override;
    using Super::start;

public:
    void start(FixedPartType, const QString & fixedPart);
    void cancel();

signals:
    void done(unsigned addressesChecked, std::shared_ptr<KeyPair>);
    void status(unsigned addressesChecked);
};

enum class Mode {
    GenerateOne,
    PrepareGenerateWithFixedPart,
    GeneratingWithFixedPart,
};

class CreateAccountView: public QWidget {
    Q_OBJECT

public:
    using This = CreateAccountView;
    explicit CreateAccountView(MainWindow * = nullptr);
    ~CreateAccountView();

private:
    Database &              database;
    AddressGenerator        generator;
    QElapsedTimer           generatorTimer;
    Mode                    mode;
    QString                 secretKey;
    Ui::CreateAccountView * ui;
    MainWindow &            window;

    void generateOne();
    void switchMode(Mode mode);
    void startGenerator(FixedPartType, const QString & fixedPart);

private slots:
    void copySecretKey();
    void save();
    void showGeneratorStatus(unsigned addressesChecked);
    void startStopGenerate();
    void updateUi();
};

#endif // CREATEACCOUNTVIEW_H
