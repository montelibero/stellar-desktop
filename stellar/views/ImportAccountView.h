#ifndef IMPORTACCOUNTVIEW_H
#define IMPORTACCOUNTVIEW_H

#include <QWidget>

#include "MainWindow.h"

namespace Ui {
    class ImportAccountView;
}

class ImportAccountView : public QWidget
{
    Q_OBJECT

public:
    using This = ImportAccountView;
    explicit ImportAccountView(MainWindow * = nullptr);
    ~ImportAccountView();

private:
    Database & database;
    Ui::ImportAccountView * ui;

private slots:
    void revalidate();
};

#endif // IMPORTACCOUNTVIEW_H
