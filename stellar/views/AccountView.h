#ifndef ACCOUNTVIEW_H
#define ACCOUNTVIEW_H

// Qt
#include <QWidget>

// project
#include "MainWindow.h"

namespace Ui {
    class AccountView;
}

class AccountView: public QWidget {
    Q_OBJECT
    using This = AccountView;

public:
    explicit AccountView(MainWindow *, Account const &);
    ~AccountView();

private:
    Account             account;
    Ui::AccountView *   ui;
    MainWindow &        window;

private slots:
    void deleteAccount();
    void goBack();
    void refresh();
    void rename();
    void setBalances(QList<Balance> const &);
};

#endif // ACCOUNTVIEW_H
