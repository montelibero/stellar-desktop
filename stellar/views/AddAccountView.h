#ifndef ADDACCOUNTVIEW_H
#define ADDACCOUNTVIEW_H

#include <QWidget>

#include "MainWindow.h"

namespace Ui {
    class AddAccountView;
}

class AddAccountView : public QWidget
{
    Q_OBJECT

public:
    explicit AddAccountView(MainWindow * = nullptr);
    ~AddAccountView();

private:
    Ui::AddAccountView * ui;
};

#endif // ADDACCOUNTVIEW_H
