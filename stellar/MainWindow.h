#ifndef MAINWINDOW_H
#define MAINWINDOW_H

// Qt
#include <QMainWindow>

// StellarQtSQK
#include <server.h>

// project
#include "Database.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

struct MainWindowPimpl;

class MainWindow: public QMainWindow {
    Q_OBJECT

public:
    Database    database;
    Server      stellarHorizonServer;

    MainWindow(bool production);
    ~MainWindow();

private:
    std::unique_ptr<MainWindowPimpl> p;
    void setupUi(bool production);
    void setupToolBar();
};

#endif // MAINWINDOW_H
