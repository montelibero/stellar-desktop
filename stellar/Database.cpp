// Qt
#include <QDir>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QStandardPaths>

// project
#include "Database.h"
#include "functools.h"
#include "types.h"

void assertQuery(QSqlQuery const & query) {
    if (query.lastError().isValid())
        throw SqlQueryException{
            .executedQuery  = query.executedQuery(),
            .lastError      = query.lastError(),
            .lastQuery      = query.lastQuery(),
        };
}

QSqlQuery
execute(
    QSqlDatabase & db, QString const & code, QVariantMap const & bindings = {}
) {
    QSqlQuery q(db);
    q.prepare(code);
    for (const auto [key, value] : bindings.asKeyValueRange())
        q.bindValue(key, value);
    q.exec();
    assertQuery(q);
    return q;
}

void open(QSqlDatabase & db) {
    if (not db.isOpen()) {
        const QDir dbdir {
            QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)
        };
        if (not dbdir.exists())
            dbdir.mkpath(".");
        db.setDatabaseName(dbdir.filePath("accounts.db"));
        db.open();
        qDebug() << db;
        Q_ASSERT(db.isOpen());
    }
}

Database::Database():
    state{QSqlDatabase::addDatabase("QSQLITE")}
{
    open(state);
    execute(
        state,
        "CREATE TABLE IF NOT EXISTS Account ("
            "balances BLOB NULL," // JSON
            "name TEXT NOT NULL,"
            "publicKey TEXT NOT NULL PRIMARY KEY"
        ");"
    );
}

Database::~Database() {
}

QByteArray balancesToJson(QList<Balance> const & balances) {
    return
        QJsonDocument(
            map<QJsonArray>(
                [](auto const & balance){
                    QJsonObject r{{"balance", balance.balance.string}};
                    if (!balance.asset_issuer.isEmpty()) {
                        r["asset_code"]     = balance.asset_code;
                        r["asset_issuer"]   = balance.asset_issuer;
                    }
                    return r;
                },
                balances
            )
        )
        .toJson(QJsonDocument::Compact);
}

QList<Balance> balancesFromJson(const QByteArray & json) {
    auto const balancesJson = QJsonDocument::fromJson(json).array();
    QList<Balance> balances;
    for (auto const & balanceJson : balancesJson) {
        auto const balanceObj = balanceJson.toObject();
        balances += Balance{
            .balance        = {balanceObj["balance"]     .toString()},
            .asset_code     =  balanceObj["asset_code"]  .toString(),
            .asset_issuer   =  balanceObj["asset_issuer"].toString(),
        };
    }
    return balances;
}

QList<Account> Database::getAccounts() {
    QList<Account> accounts;
    auto q = execute(state, "SELECT * FROM Account;");
    while (q.next())
        accounts += Account{
            .balances   = balancesFromJson(q.value("balances").toByteArray()),
            .name       = q.value("name")       .toString(),
            .publicKey  = q.value("publicKey")  .toString(),
        };
    return accounts;
}

Account Database::addAccount(const QString & publicKey, const QString & name) {
    execute(
        state,
        "INSERT OR IGNORE INTO Account"
            "(name, publicKey) VALUES (:name, :publicKey);",
        {{":name", name}, {":publicKey", publicKey}}
    );
    return {.name = name, .publicKey = publicKey};
}

void Database::deleteAccount(Account const & account) {
    execute(
        state,
        "DELETE FROM Account WHERE publicKey = :publicKey;",
        {{":publicKey", account.publicKey}}
    );
}

void Database::setAccountName(Account & account, QString const & name) {
    account.name = name;
    execute(
        state,
        "UPDATE Account SET name = :name WHERE publicKey = :publicKey",
        {{":name", name}, {":publicKey", account.publicKey}}
    );
}

void Database::setBalances(Account & account, const QList<Balance> & balances) {
    account.balances = balances;
    execute(
        state,
        "UPDATE Account SET balances = :balances WHERE publicKey = :publicKey",
        {
            {":balances",   balancesToJson(balances)},
            {":publicKey",  account.publicKey},
        }
    );
}
