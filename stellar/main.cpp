#include <QApplication>
#include <QLocale>
#include <QSqlError>
#include <QTranslator>

#include "MainWindow.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        const QString baseName = "stellar_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + baseName)) {
            app.installTranslator(&translator);
            break;
        }
    }

    const bool production =
        app.arguments().contains("--test")
        ?   false
        :   app.arguments().contains("--production")
            ?   true
            :
                #ifdef QT_DEBUG
                    false
                #else
                    true
                #endif
            ;

    try {
        MainWindow w{production};
        w.show();
        return app.exec();
    } catch (SqlQueryException const & e) {
        qDebug()
            << "SqlQueryException:"
            << "  executedQuery:" << e.executedQuery
            << "  lastError:"     << e.lastError
            << "  lastQuery:"     << e.lastQuery;
    }
    return 1;
}
