#ifndef RICHBUTTON_H
#define RICHBUTTON_H

#include <QtWidgets>

struct RichButtonPimpl;

class RichButton : public QPushButton {
    Q_OBJECT

private:
    using Super = QPushButton;
    std::unique_ptr<RichButtonPimpl> p;

public:
    explicit RichButton(QWidget * = nullptr);
    ~RichButton();
    void setText(const QString &);
};

#endif // RICHBUTTON_H
